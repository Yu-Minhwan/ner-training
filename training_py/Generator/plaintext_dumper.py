#-*- coding: utf-8 -*-

# for development
import sys, pprint
# for utility
import random, time, os
from glob import glob

config = {
        "criteria": {
            "min_rating" : 3.0,
            "min_length" : 50
        },
        "index": {
            "rating": 8,
            "review": 10,
            },
        "review_cnt": 10000,

        "target_dir": "./review_data/sanjose/"
    }

ROOTS = [
'/media/yuminhwan/9Platters/yelp/Integration/20150121_debup/**/**/ca/san_jose/',
]

"""
    read file system to find what we want to handle
"""
def load_extractFiles():
    result = list()

    for ROOT in ROOTS:
        files = glob(ROOT + "/*.txt")
        result.extend(files)

    return result

"""
    parsing the extract files (only fetch review plaintext)
"""
def parsing_extractFile(file_path):
    reviews = list()
    lineNum = 0

    f = open(file_path)
    for line in f:
        lineNum += 1

        if lineNum == 1 or lineNum == 2:
            continue

        metas = line.split('\t')

        rating = metas[config['index']['rating']]
        review = metas[config['index']['review']]

        if len(rating) == 0:
            rating = '0.0'

        if (rating < config['criteria']['min_rating'] or
            len(review) < config['criteria']['min_length']):
            continue

        reviews.append(review)

    return reviews

"""
    generate random values(integer)
    -> which review will be written
"""
def generate_randomInt(start, end, count):
    numbers = set()
    if end < count :
        print "### The number of element is lower \
than what you want to generate...."
        sys.exit()

    while True:
        num = random.randint(start, end)
        if not num in numbers:
            numbers.add(num)
        if len(numbers) == count:
            break;

    return numbers

"""
    wrtie the selected reviews
"""
def write_plaintext_reviews(reviews):
    file_path = config['target_dir'] + time.strftime("%Y%m%d_%H%M%S", time.localtime()) + ".txt"
    dir_path = os.path.dirname(file_path)

    try:
        os.stat(dir_path)
    except:
        os.mkdir(dir_path)

    print "# dump file path: " + file_path
    f = open(file_path, 'a')

    for review in reviews:
        f.write(review)
        f.write('\n')

    f.close()

"""
    Start point
"""
if __name__ == '__main__':
    files = load_extractFiles()

    print "# The number of the extracted files: " + str(len(files))
    execute_cnt = 0
    reviews = list()
    for f in files:
        execute_cnt += 1
        reviews.extend(parsing_extractFile(f))
        if execute_cnt % 100 == 0:
            print execute_cnt

    review_indices = generate_randomInt(0, len(reviews), config['review_cnt'])
    reviews = [ reviews[reviewIdx] for reviewIdx in review_indices]

    write_plaintext_reviews(reviews)
