import copy

def __nominal__(val, nominal):

    if (val not in nominal) or (val == None):
        return nominal[0]

    idx = nominal.index(val)
    if idx == len(nominal)-1 :
        return val
    else:
        return nominal[idx+1]


def __boolean__(val):

    return __nominal__(val, ['true', 'false'])

def __numeric__(val, setting):
    if val == None:
        return setting['min']

    _delta = setting['delta']
    _max = setting['max']

    n_val = val + _delta

    if n_val > _max :
        return val
    else:
        return n_val

def init_featureset(featureset):
    for key in featureset.keys():
        featureset[key] = init_feature(featureset[key])[1]
    return featureset

def init_feature(feature):
    if 'val' in feature:
        feature['val'] = None

    feature = next_feature(feature)
    return feature

def next_feature(feature):

    # feature type
    ft = feature['type']
    # current feature value
    if 'val' not in feature:
        fv = None
    else:
        fv = feature['val']

    if (ft is int) or (ft is float) or (ft is long):
        next_val = __numeric__(fv, feature['setting'])
    elif ft is bool:
        next_val = __boolean__(fv)
    elif ft is list:
        next_val = __nominal__(fv, feature['nominal'])
    else:
        print 'ERROR: invalid feature type'

    if next_val == fv:
        return False, feature
    else:
        feature['val'] = next_val
        return True, feature


def cleaning(featureset):
    result = dict()

    for key in featureset.keys():
        result[key] = featureset[key]['val']

    return result

if __name__ == '__main__':
    test_dump = {
        'nominalA': {
            'type': list,
            'nominal': ['a1', 'a2', 'a3']
        },
        'nominalB': {
            'type': list,
            'nominal': ['b1', 'b2', 'b3']
        },
        'nominalC': {
            'type': list,
            'nominal': ['c1', 'c2', 'c3']
        },
    }

    test_dump = init_featureset(test_dump)
    keys = test_dump.keys()

    for f_range in range(0, len(keys)):
        shuffle_keys = keys[:f_range]
        moving_key = keys[f_range]

        moving_f = test_dump[moving_key]
        if f_range == 0:
            moving_f['val'] = None

        print shuffle_keys
        while True:
            r,f = next_feature(moving_f)

            if r:
                moving_f = f
                print cleaning(test_dump)
            else:
                break

        moving_f = init_feature(moving_f)
        print "#################"
