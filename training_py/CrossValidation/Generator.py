from nltk.tokenize.stanford import StanfordTokenizer as Tokenizer
import os, glob, pprint

import util

def init_tmp():
  util.rm_dir('tmp/review')
  util.rm_dir('tmp/token')

  os.mkdir('tmp/review')
  os.mkdir('tmp/token')


def token_files(config):
  init_tmp()
  review_f = open(config.Path['dataset-dir'] + 'reviews.txt')

  review_cnt = 0
  # gen token files
  for review in review_f:
    review = review.strip()
    review_cnt += 1

    txtfile_path = os.path.join('tmp', 'review', str(review_cnt) + '.txt')
    tokfile_path = os.path.join('tmp', 'token', str(review_cnt) + '.tok')

    tmp_f = open(os.path.join('tmp', 'review', str(review_cnt) + '.txt'), 'a')
    tmp_f.write(review)
    tmp_f.close()

    cmd = 'java -cp ' + config.Path['stanford-ner-dir'] + 'stanford-ner.jar edu.stanford.nlp.process.PTBTokenizer ' + txtfile_path + ' > ' + tokfile_path
    print cmd
    util.cmd(cmd)


def training_data(config):
  tokencounter = dict()

  # get the number of token file
  tokenfile_paths = glob.glob('tmp/token/*.tok')
  review_cnt = len(tokenfile_paths)

  # write all token in a single training dataset file
  trainingdata_f = open(config.Path['dataset-dir'] + 'unlabel.tsv', 'a')

  for tokenfile_name in range(1, review_cnt+1):
    tokenfile_path = os.path.join('tmp', 'token', str(tokenfile_name) + '.tok')
    tokenfile = open(tokenfile_path)

    token_cnt = 0
    for token in tokenfile:
      token_cnt += 1
      trainingdata_f.write(token.strip() + '\t' + 'O\n')

    tokenfile.close()

    # save how many token is in the review
    tokencounter[tokenfile_name] = token_cnt

  trainingdata_f.close()

  # wrtie the number of token each review
  tokencounter_f = open(config.Path['dataset-dir'] + 'tokencnt.txt', 'a')

  for i in range(1, review_cnt+1):
    tokencounter_f.write( str(tokencounter[i]) )
    tokencounter_f.write('\n')

  tokencounter_f.close()


def crossvalidation_dataset(config):
  # gather and split  tokens in each review
  #  |- review1 - [token11, token12, token13, ...]
  #  |- review2 - [token21, token22, token23, ...]
  reviews = list()

  tokencounter_f = open(config.Path['dataset-dir'] + 'tokencnt.txt')
  label_f = open(config.Path['dataset-dir'] + 'label.tsv')

  for tokencnt in tokencounter_f :
    token_cnt = int(tokencnt)

    read_cnt = 0
    review = list()

    for line in label_f :
      review.append(line.strip())

      read_cnt += 1
      if read_cnt == token_cnt:
        reviews.append(review)
        break

  tokencounter_f.close()
  label_f.close()

  # split into training and testing dataset
  window_size = len(reviews) / config.FoldCnt

  for i in range(0, config.FoldCnt) :
    # set the index range of testing dataset
    start_idx = i * window_size
    end_idx = (i + 1) * window_size - 1

    # initialize training and testing dataset
    training = list()
    testing = list()

    for reviewIdx in range(0, len(reviews)) :
      review = reviews[reviewIdx]

      if reviewIdx >= start_idx and reviewIdx <= end_idx :
        print reviewIdx
        testing.append(review)
      else:
        training.append(review)

    # write each fold's training, testing dataset
    CVdir_path = os.path.join(config.Path['dataset-dir'], 'CV', str(i))
    util.mkdir_p(CVdir_path)

    testing_f = open(CVdir_path + '/testing.tsv', 'w')
    for review in testing:
      testing_f.write( '\n'.join(review) )
      testing_f.write( '\n' )
    testing_f.close()

    training_f = open(CVdir_path +'/training.tsv', 'w')
    for review in training :
      training_f.write( '\n'.join(review) )
      training_f.write( '\n' )
    training_f.close()


def training_property(config):
  # create common training property values according to config.Properties
  prop_common = ''

  properties = config.Properties
  for prop_name in properties.keys():
    prop_val = properties[prop_name]

    if prop_val != None:
      prop_common += prop_name + '=' + str(prop_val) + '\n'

  # wrtie property files for each CV step
  for i in range(0, config.FoldCnt):
    CVdir_path = config.Path['dataset-dir'] + 'CV/' + str(i)


    prop_str = prop_common
    prop_str = prop_common + \
        'trainFile=' + CVdir_path + '/training.tsv\n' + \
        'serializeTo=' + CVdir_path + '/ner-model.ser.gz'

    prop_f = open(CVdir_path + '/training.prop', 'w')
    prop_f.write(prop_str)
    prop_f.close()
