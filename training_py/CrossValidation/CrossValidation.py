import util, os, json

def training(config):
    print '# trainig ner-models...'
    # traning ner-model for each training data
    for i in range(0, config.FoldCnt):
        CVdir_path = config.Path['dataset-dir'] + 'CV/' + str(i)

        propfile_path = CVdir_path + '/training.prop'
        print propfile_path
        cmd = ' '.join([
                'java', '-cp',
                '../stanford-ner-2015-04-20/stanford-ner.jar',
                'edu.stanford.nlp.ie.crf.CRFClassifier',
                '-prop',
                propfile_path])

        util.cmd(cmd)

def testing(config):
    print '# testing ner-models...'
    # testing for each ner-model
    for i in range(0, config.FoldCnt):
        CVdir_path = os.path.join(config.Path['dataset-dir'], 'CV', str(i))

        modelfile_path = CVdir_path + '/ner-model.ser.gz'
        testingfile_path = CVdir_path + '/testing.tsv'
        logfile_path = CVdir_path + '/predict_result.txt'
        performancefile_path = CVdir_path + '/predict_perfomance.txt'

        cmd = ' '.join([
                'java', '-cp',
                '../stanford-ner-2015-04-20/stanford-ner.jar',
                'edu.stanford.nlp.ie.crf.CRFClassifier',
                '-loadClassifier',
                modelfile_path,
                '-testFile',
                testingfile_path])
        util.cmd(cmd, logfile_path, performancefile_path)

def cal_performance(config):
    avgPrecision = 0
    avgRecall = 0
    avgF1 = 0
    for i in range(0, config.FoldCnt):
        CVdir_path = os.path.join(config.Path['dataset-dir'], 'CV', str(i))
        performancefile_path = CVdir_path + '/predict_perfomance.txt'
        f = open(performancefile_path)
        for line in f:
            if 'Totals' in line:
                (name, p, r, f, g)= line.strip().split("\t", 4)
                avgPrecision = avgPrecision + float(p)
                avgRecall = avgRecall + float(r)
                avgF1 = avgF1 + float(f)

    avgPrecision = avgPrecision/config.FoldCnt
    avgRecall =  avgRecall/config.FoldCnt
    avgF1 = avgF1/config.FoldCnt

    util.mkdir_p('../result')
    wf = open('../result/' + config.Path["executedate"] + '.txt', 'w')

    wf.write(str(avgPrecision) + "\t")
    wf.write(str(avgRecall) + "\t")
    wf.write(str(avgF1) + "\t\n")

    wf.write(json.dumps(config.Properties, indent=4, sort_keys=True))
    wf.close()
