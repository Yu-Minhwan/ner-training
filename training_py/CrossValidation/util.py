import os, errno, sys, subprocess, shutil

def rm(path):
  if os.path.isfile(path):
    os.remove(path)

def rm_dir(path):
  shutil.rmtree(path, ignore_errors=True)

def mkdir_p(path):
  try:
      os.makedirs(path)
  except OSError as exe:
      if exe.errno == errno.EEXIST and os.path.isdir(path):
          pass
      else:
          print "mkdir (" + path + ") fail.."
          sys.exit()

def cmd(cmd, log_path=None, err_path=None):
  print cmd
  if log_path == None:
    ret = subprocess.call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  else:
    stdout_f = open(log_path, 'w')
    stderr_f = open(err_path, 'w')

    ret = subprocess.call(cmd, stdout=stdout_f, stderr=stderr_f, shell=True)

    stdout_f.close()
    stderr_f.close()

  if ret > 0:
    print "Warning - result was %d" % ret
    sys.exit()
