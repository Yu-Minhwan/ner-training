import os

"""
Data Path
"""
Path = {
    'stanford-ner-dir': '../stanford-ner-2015-04-20/',
    #'dataset-dir': '../DataSet/20150630_134255_Sanjose-200/',
    #'dataset-dir': '../DataSet/20150623_194506_SanFrancisco-300/',
    'dataset-dir': '../DataSet/20150702_sanfrancisco300-sanjose200/',

    'executedate': '',
}

FoldCnt = 1

AutoLabel = ['spicy burgers']

"""
Feature Config

properties = {
# Description
    featureName(property key) : value
    ...
    ...
}
"""
Properties = {
# word=0,answer=1
# word=0,tag=1,answer=2
        'map' : 'word=0,answer=1',

# Gives you feature for w
        'useWord': 'ture',
# If non-null, treat as a sequence of comma separated integer bounds,
# where items above the previous bound up to the next bound are binned Len-range
        'useBinnedLength': None,
# Make features from letter n-grams, i.e., substrings of the word
        'useNGrams': 'true',
# Make features from letter n-grams only lowercase
        'lowercaseNGrams': 'false',
# Remove hyphens before making features from letter n-grams
        'dehyphenateNGrams': 'false',
# Conjoin word shape and n-gram features
        'conjoinShapeNGrams': 'false',
# Use letter n-grams for the previous and current words in the CpC clique.
# This feature helps languages such as Chinese, but not so much for English
        'useNeighborNGrams': 'false',
# Gives you feature for (pw,c), and together with other options
# enables other previous features, such as (pt,c) [with useTags)
        'usePrev': 'true',
# Gives you feature for (nw,c), and together with other options
# enables other next features, such as (nt,c) [with useTags)
        'useNext': 'true',
# Gives you features for (t,c), (pt,c) [if usePrev], (nt,c) [if useNext]
        'useTags': 'false',
# Gives you features for (pw, w, c) and (w, nw, c)
        'useWordPairs': 'false',
# If true, use gazette features (defined by other flags)
        'useGazettes': 'false',
# The value can be one or more filenames
        'gazette': './gazettes.txt',
# If true, a gazette feature fires when any token of a gazette entry matches
        'sloppyGazette': 'true',
# If true, a gazette feature fires when all tokens of a gazette entry match
        'cleanGazette': 'true',
# Either "none" for no wordShape use, or the name of a word shape
# function recognized by WordShapeClassifier.lookupShaper(String)
        'wordShape': 'is2useLC',
# Does not use any class combination features if this is false
        'useSequences': 'true',
# Does not use any class combination features
# using previous classes if this is false
        'usePrevSequences': 'true',
# Does not use any class combination features
# using next classes if this is false
        'useNextSequences': 'false',
# Use plain higher-order state sequences out to minimum of length or maxLeft
        'useLongSequences': 'false',
# Use extra second order class sequence features when previous is CoNLL boundary,
# so entity knows it can span boundary.
        'useBoundarySequences': 'false',
# Use first, second, and third order classa and
# tag sequence interaction features
        'useTaggySequences': 'false',
# Add in sequences of tags with just current class features
        'useExtraTaggySequences': 'false',
# Add in terms that join sequences of 2 or 3 tags with the current shape
        'useTaggySequencesShapeInteraction': 'false',
# As an override to whatever other options are in effect,
# deletes all features other than C and CpC clique features when building the classifier
        'strictlyFirstOrder': 'false',
# If set, convert the labeling of classes (but not the background)
# into one of several alternate encodings (IO, IOB1, IOB2, IOE1, IOE2, SBIEO, with a S(ingle),
# B(eginning), E(nding), I(nside) 4-way classification for each class.
# By default, we either do no re-encoding, or the CoNLLDocumentIteratorFactory
# does a lossy encoding as IO. Note that this is all CoNLL-specific,
# and depends on their way of prefix encoding classes, and is only implemented by the
# CoNLLDocumentIteratorFactory.
        'entitySubclassification': None,

        'useSum': 'false',

# Convergence tolerance in optimization
        'tolerance': None,

# print out all the features generated by the classifier for a dataset to
# a file based on this name (starting with "features-", suffixed "-1" and "-2" for train and test).
# This simply prints the feature names, one per line.
        'printFeatures': None,

# Print out features for only the first this many datums, if the value is positive.
        'printFeaturesUpto': -1,

# Gives you features (pt, t, nt, c), (t, nt, c), (pt, t, c)
        'useSymTags': None,

# Gives you features (pw, nw, c)
        'useSymWordPairs': None,

# Style in which to print the classifier.
# One of: HighWeight, HighMagnitude, Collection, AllWeights, WeightHistogram
        'printClassifier': None,

# A parameter to the printing style, which may give, for example the number of parameters to print
        'printClassifierParam': 100,

# If true, (String) intern read in data and classes and feature
# (pre-)names such as substring features
        'intern': None,

# If true, intern all (final) feature names (if only current word and ngram features are used,
# these will already have been interned by intern, and this is an unnecessary no-op)
        'intern2': None,

# If true, record the NGram features that correspond to a String
# (under the current option settings) and reuse rather than recalculating if the String is seen again.
        'cacheNGrams': None,

        'selfTest': None,

# Do not include character n-gram features for n-grams that contain neither
# the beginning or end of the word
        'noMidNGrams': None,

# If this number is positive, n-grams above this size will not be used in the model
        'maxNGramLeng': 6,

# useReverse
        'useReverse': 'false',

# If true, rather than undoing a recoding of entity tag subtypes
# (such as BIO variants), just leave them in the output.
        'retainEntitySubclassification': 'false',

# Include the lemma of a word as a feature.
        'useLemmas':'false',

# Include the previous/next lemma of a word as a feature.
        'usePrevNextLemmas': 'false',

# Include the lemma of a word as a feature.
        'useLemmaAsWord': 'false',

# If this is true, some words are normalized: day and month names are lowercased
# (as for normalizeTimex) and some British spellings are mapped to American English spellings (e.g., -our/-or, etc.).
        'normalizeTerms': 'false',

# If this is true, capitalization of day and month names is normalized to lowercase
        'normalizeTimex': 'false',

        'useNB': 'false',

# Use basic zeroeth order word shape features
        'useTypeSeqs': 'true',

# Add additional first and second order word shape features
        'useTypeSeqs2': 'true',

# Adds one more first order shape sequence
        'useTypeSeqs3': 'false',

# Include in features giving disjunctions of words anywhere in the left or
# right disjunctionWidth words (preserving direction but not position)
        'useDisjunctive': 'false',

# The number of words on each side of the current word that are included in the disjunction features
        'disjunctionWidth': 4,

# Include in features giving disjunctions of words anywhere in the left or
# right disjunctionWidth words (preserving direction but not position) interacting with the word shape of the current word
        'useDisjunctiveShapeInteraction': 'false',

# Include in features giving disjunctions of words anywhere in the left or
# right wideDisjunctionWidth words (preserving direction but not position)
        'useWideDisjunctive': 'false',

# The number of words on each side of the current word that are included in the disjunction features
        'wideDisjunctionWidth': 4,

# Use combination of position in sentence and class as a feature
        'usePosition': 'false',

# Use combination of initial position in sentence and class (and word shape) as a feature.
# (Doesn't seem to help.)
        'useBeginSent': 'false',

# Include features giving disjunctions of word shapes anywhere in the left or
# right disjunctionWidth words (preserving direction but not position)
        'useDisjShape': 'false',

# Include a feature for the class (as a class marginal). Puts a prior on the classes which is
# equivalent to how often the feature appeared in the training data.
        'useClassFeature': 'true',

# Conjoin shape with tag or position
        'useShapeConjunctions': 'false',

# Include word and tag pair features
        'useWordTag': 'false',

# Iff the prev word is of length 3 or less, add an extra feature that combines
# the word two back and the current word's shape. Weird!
        'useNextRealWord': 'false',

# Match a word against a list of name titles (Mr, Mrs, etc.). Doesn't really seem to help.
        'useTitle': 'false',

# Match a word against a better list of English name titles (Mr, Mrs, etc.). Still doesn't really seem to help.
        'useTitle2': 'false',

# Load a file of distributional similarity classes (specified by distSimLexicon) and use it for features
        'useDistSim': 'false',

# The file to be loaded for distsim classes.
        'distSimLexicon': None,

# Files should be formatted as tab separated rows where each row is a word/class pair.
# alexclark=word first, terrykoo=class first
        'distSimFileFormat': 'alexclark',

# This is a very engineered feature designed to capture multiple references to names.
# If the current word isn't capitalized, followed by a non-capitalized word, and preceded by
# a word with alphabetic characters, it returns NO-OCCURRENCE-PATTERN. Otherwise,
# if the previous word is a capitalized NNP, then if in the next 150 words you find this PW-W sequence,
# you get XY-NEXT-OCCURRENCE-XY, else if you find W you get XY-NEXT-OCCURRENCE-Y. Similarly for backwards and
# XY-PREV-OCCURRENCE-XY and XY-PREV-OCCURRENCE-Y. Else (if the previous word isn't a capitalized NNP),
# under analogous rules you get one or more of X-NEXT-OCCURRENCE-YX, X-NEXT-OCCURRENCE-XY, X-NEXT-OCCURRENCE-X,
# X-PREV-OCCURRENCE-YX, X-PREV-OCCURRENCE-XY, X-PREV-OCCURRENCE-X.
        'useOccurrencePatterns': 'false',

# Some first order word shape patterns.
        'useTypeySequences': 'true',

# If true, any features you include in the map will be incorporated into the model with
# values equal to those given in the file; values are treated as strings unless you use the
# "realValued" option (described below)
        'useGenericFeatures': 'false',

# Print out all feature/class pairs and their weight, and then for each input data point,
# print justification (weights) for active features
        'justify': 'false',

# For the CMMClassifier (only) if this is true then the Scorer normalizes scores as probabilities
        'normalize': 'false',

# Use a Huber loss prior rather than the default quadratic loss.
        'useHuber': 'false',

# Use a Quartic prior rather than the default quadratic loss.
        'useQuartic': 'false',

# sigma
        'sigma': 1.0,

#  Used only as a parameter in the Huber loss: this is the distance from 0
# at which the loss changes from quadratic to linear
        'epsilon': 0.01,

# beamSize
        'beamSize': 30,

# The number of things to the left that have to be cached to run the Viterbi algorithm:
# the maximum context of class features used.
        'maxLeft': 1,

# The number of things to the right that have to be cached to run the Viterbi algorithm:
# the maximum context of class features used. The maximum possible clique size to use is (maxLeft + maxRight + 1)
        'maxRight': 2,

# If true, any features you include in the map will be incorporated into the model with values
# equal to those given in the file; values are treated as strings unless you use the "realValued" option (described below)
        'dontExtendTaggy': 'false',

# The number of folds to use for cross-validation. CURRENTLY NOT IMPLEMENTED.
        'numFolds': 1,

# The starting fold to run. CURRENTLY NOT IMPLEMENTED.
        'startFold': 1,

# The last fold to run. CURRENTLY NOT IMPLEMENTED.
        'endFold': 1,

# Whether to merge B- and I- tags.
        'mergeTags': 'false',

# Whether or not to split the data into separate documents for training/testing
        'splitDocuments': 'true',

# maxDocSize
        'maxDocSize': 10000
}



