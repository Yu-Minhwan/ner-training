import pprint, time, os, sys

from CrossValidation import Generator, CrossValidation, util
from Generator import Labeler

import config

ops = [
        'gen-trainingdata',
        'auto-labeling',
        'gen-CVdata',
        'execute-CV',
        'find-bestmodel'
        ]

class Executer():
    def __init__(self, config, op=None):
      self.config = config

      if op == 'gen-trainingdata':
        self.gen_trainingdata()
      elif op == 'gen-CVdata':
        self.gen_CVdata()
      elif op == 'execute-CV':
        self.execute_CV()
      elif op == 'find-bestmodel':
        self.find_bastmodel()
      else:
        print "### ERROR ###  invalid operation\nPlz just input 'python execute.py'"

    def gen_trainingdata(self):
      Generator.token_files(self.config)
      Generator.training_data(self.config)

    def gen_CVdata(self):
      Generator.crossvalidation_dataset(self.config)

    def execute_CV(self):
      Generator.training_property(self.config)
      CrossValidation.training(self.config)
      CrossValidation.testing(self.config)
      CrossValidation.cal_performance(self.config)

    def auto_labeling(self):
      Labeler.auto_labeling(self.config.AutoLabel, self.config)

    def find_bestmodel(self):
      pass

if __name__ == '__main__':
    if len(sys.argv) < 2 :
      print "### ERROR ### invalid execution format : python [this file name] [op]"
      print "\t ops"
      for op in ops:
        print "\t|- " + op

    else:
      config.Path['executedate'] = time.strftime("%Y%m%d_%H%M%S", time.localtime())

      e = Executer(config, sys.argv[1])
