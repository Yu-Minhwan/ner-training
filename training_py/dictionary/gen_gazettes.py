import os
from glob import glob

gazettes = list()

dicfile_paths = glob('./*.dict')
for dicfile_path in dicfile_paths:
    f = open(dicfile_path)

    classname = os.path.splitext(os.path.basename(dicfile_path))[0]

    for line in f:
        line = line.strip()
        gazettes.append(classname + '\t' + line)

    f.close()

gazettes_f = open('../gazettes.txt', 'w')
for gazette in gazettes:
    gazettes_f.write(gazette)
    gazettes_f.write('\n')
gazettes_f.close()
